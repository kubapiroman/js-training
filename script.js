/*LESSON 1*/

// const food = 'gruz'

// let age = 24

// const name = 'Kuba'

// console.log(`Hello, my name is ${name}, I love to eat ${food}. I'm ${age} years old.`);


/*LESSON 2*/


// const favColor = 'red'
// const favMeal = 'schabowy'
// const favDrink = 'pepsi'
// const age = 123
// const name = 'Kuba'



// console.log(`Hi, my name is ${name}, I'm ${age} years old.`);



/*LESSON 3  Data types*/


// STRING - 'Hello'
// const name = 'Kuba'

// NUMBER - 1234
// const age = 1234.5

// BOOLEAN - true/false
// const isTrue = true

// NULL & UNDEFINED 
// const test = null



/*LESSON 4   Arithmetic operators*/


// const add = 5 + 6
// console.log(`Addition result: ${add}`);


// const substract = 6 - 5
// console.log(`Substraction result: ${substract}`);


// const multiply = 5 * 6
// console.log(`Multiplication result: ${multiply}`);


// const divide = 15 / 3
// console.log(`Division result: ${divide}`);


// const modulo = 15 % 7
// console.log(`Modulo result: ${modulo}`);

// ++ increment
// -- decrement

// let num = 4
// console.log(num);
// num++
// num++
// num++
// console.log(num);
// num--
// num--
// num--
// num--
// num--
// console.log(num);



/*LESSON 4    Assignment operators*/

// let x = 8
// let y = 10

// x = x + y          //same as below
// x += y

// x *= y  same as  x = x * y
// x /= y  same as  x = x / y
// x %= y  same as  x = x % y


// console.log(x);



/* Lesson 5    Comparsion operators*/


// const x = 8
// const y = '10'
// const z = 9

/*

        == sprawdzamy, czy coś jest RÓWNE (sama wartość)
        === sprawdzamy czy coś jest RÓWNE (wartość i typ danych)

        != sprawdamy czy coś jest RÓŻNE (sama wartość)
        !== sprawdzamy czy coś jest RÓWNE (wartość i typ danych)

        > większy niż...
        < mniejszy niż...

        >= większy lub równy
        <= mniejszy lub równy

*/

// if(z != y) {
//     console.log('thumbs_up');
// } else {
//     console.log('thumbs_down');
// }



/*  LESSON 6    Logic operators*/


/*

    && = AND  obydwa warunki musza byc prawdziwe

    || = OR   tylko jeden warunek musi byc prawdziwy

    ! = NOT   negacja, zaprzeczenie

*/

// if(true) {
//     console.log('thumbs_up');
// } else {
//     console.log('thumbs_down');
// }


// const score = 15 % 2
// console.log(score);


// let num1 = 8
// num1++
// num1++
// console.log(num1);



/* LESSON 7 Conditions and if statement*/

// if(true) {
//     console.log('thumbs-up');
// } else {
//     console.log('thumbs_down');
// }



// if(nastawimy_budzik) {
//     zostaniemy_obudzeni
// } else {
//     bedziemy_spali_dalej
// }

// if(otworzymy_lodowke && bedzie_zarcie){
//     mozemy_zjesc
// } else {
//     bedziemy_glodni
// }

// const passLength = 3



/*requirements:

Strong password: >= 10 
Good password: 5 - 10
Weak password: <= 5
*/

/*PASSWORD APP*/

// const value = "lol4444444"

// const passwordLength = value.length

// console.log(passwordLength);




// if(passwordLength >= 10) {
//     console.log('Strong password!');
// } else if(passwordLength > 5 && passwordLength < 10) {
//     console.log('Good password');
// } else {
//     console.log('Weak password');
// }


/*   LESSON 8  Arrays*/



// const colors = ['red', 'green', 'blue']

// colors.push('gold')  - /*To nie jest zmiana zmiennej stałej a jedynie dodanie wartości. Tabeta nadal jest tabelą*/
// console.log(colors);
// console.log(colors.length);



/* LESSON 9 FOR LOOP*/

// const fruits = ['🍌', '🍎', '🍑','🍇']

// for(let i = 0; i < 4; i++) {
//     console.log(fruits[i]);
// }



/* LESSON 10 Functions*/

// function test(){
//     console.log('My first function!');
// }

// test()

// function addNumbers(x, y){
//     console.log(x, y);
//     console.log(x + y);
// }

// addNumbers(5, 10)


// function addNumbers(x, y){
//     console.log(x, y);
//     console.log(x * y);
// }


/* ZADANIE */

// const numbers = [1, 2, 3, 4, 5, 6,];

// // console.log(array);

// for(let i = 0; i < numbers.length; i++) {
//     console.log(numbers[i]);
// }

// function addNumbers(x, y, z) {
//     console.log(x + y * z);
// }

// addNumbers(1, 2, 3)


// if (numbers.length > 5 ) {
//     console.log(true);
// }   else {
//     console.log(false);
// }

// console.log(`Tablica z cyframi ma ${numbers.length} elementow.`);



// const numbers = [1, 2, 3, 4, 5, 6, 7];

// for (let i = 0; i < 6; i++) {
//     console.log(numbers[i]);
// }

// function addNumbers(x, y, z) {
//     console.log(x + y * z);
// }

// addNumbers(1, 2, 3)

// if (numbers.length > 5) {
//     console.log(true);
// }   else {
//     console.log(false);
// }

// console.log(`Tabela z cyframi ma ${numbers.length} elementów.`);





/* Pobieranie elementów + addEventListener lesson*/

// const p = document.querySelector('p')

// console.log(p);

// const btn = document.querySelector('#btn')
// console.log(btn);


// const btn1 = document.querySelector('#btn1')

// const btn2 = document.querySelector('.btn2')

// // console.log(btn1, btn2);

// function handleClick() {
//     console.log("kliknieto przycisk 1");
// }

// function addNumbers() {
//     console.log(2 + 2);
// }


// btn1.addEventListener('click', handleClick)

// btn2.addEventListener('click',addNumbers)


/* addEventListener project*/

// const redBtn = document.querySelector('#btn1')
// const blueBtn = document.querySelector('.btn2')
// const square = document.querySelector('.color')
// const removeColorBtn = document.querySelector('.remove-color')

// function redColor() {
// 	square.classList.add('red')
//     square.classList.remove('blue')
// }

// function blueColor() {
// 	square.classList.add('blue')
//     square.classList.remove('red')
// }


// function deleteColor() {
//     square.classList.remove('red')
//     square.classList.remove('blue')
// }


// redBtn.addEventListener('click', redColor)
// blueBtn.addEventListener('click', blueColor)
// removeColorBtn.addEventListener('click', deleteColor)

/* quest 2*/

// const arrowIcon = document.querySelector('.fas')
// const btn = document.querySelector('arrow')
// const img = document.querySelector('item1')

// function showImage() {
//     img.classList.toggle('show')

//     if (img.classList.contains('show')) {
//         arrowIcon.style.transform = 'rotate(180deg)'
//     }   else {
//         arrowIcon.style.transform = 'rotate(0deg)'
//     }
// }

// btn.addEventListener('click', showImage)



/*Last exercise*/

// const sizeUp = document.querySelector('.size-up')
// const sizeDown = document.querySelector('.size-down')
// const color = document.querySelector('.color')
// const p = document.querySelector('p')

// let fontSize = 20

// function bigText() {
//     fontSize += 5
//     p.style.fontSize = fontSize + 'px'
// }

// function smallText() {
//     fontSize -= 5
//     p.style.fontSize = fontSize + 'px'
// }

// function colorChange() {
//     p.style.color = 'gold'
// }


// sizeUp.addEventListener('click', bigText )
// sizeDown.addEventListener('click', smallText)
// color.addEventListener('click', colorChange)

/* LESSON with KAMIL*/

function addNumbers(firstNumber, secondNumber) {
	return firstNumber + secondNumber
}

// console.log(addNumbers(2, 3))
// console.log(addNumbers(5, 7))
// console.log(addNumbers(4, 1))

function dayOfTheWeek(dayNumber) {
	if (dayNumber === 1) {
		return 'poniedziełek'
	} else if (dayNumber === 2) {
		return 'wtorek'
	} else {
		return 'inny dzien'
	}
    console.log('koniec');
}



console.log();
console.log(dayOfTheWeek(1));
console.log(dayOfTheWeek(2));  

console.log(`Dzisiaj jest ${dayOfTheWeek(1)}, a jutro jest ${dayOfTheWeek(2)}.`);

const myDay = dayOfTheWeek(1)
console.log(myDay);

const todaysDay = new Date().getDay()
console.log(todaysDay);

console.log(dayOfTheWeek(todaysDay));
